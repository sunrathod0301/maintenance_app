package com.sunarcade.maintenance_app.repository;

import com.sunarcade.maintenance_app.model.FlatShopRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlatShopRepository extends JpaRepository<FlatShopRecord, Long>
{

}
