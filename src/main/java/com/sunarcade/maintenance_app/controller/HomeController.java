package com.sunarcade.maintenance_app.controller;

import com.sunarcade.maintenance_app.model.FlatShopRecord;
import com.sunarcade.maintenance_app.services.FlatShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class HomeController
{
    @Autowired
    private FlatShopService service;

    @GetMapping("/")
    public String getAllFlatShops(Model model)
    {
        return findPaginated(1, "sinkingFund","asc",model);
    }

    @GetMapping("/page/{pageNo}")
    public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
                                @RequestParam("sortField") String sortField,
                                @RequestParam("sortDir") String sortDir,
                                Model model)
    {
        int pageSize=10;
        Page<FlatShopRecord> page = service.findPaginated(pageNo,pageSize, sortField, sortDir);
        List<FlatShopRecord> listFlatShops = page.getContent();
        System.out.println(listFlatShops);
        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("listFlatShops", listFlatShops);

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equalsIgnoreCase("asc") ? "desc" : "asc");

        return "index";
    }

    @GetMapping("/addNewRecordForm")
    public String showNewRecordForm(Model model)
    {
        //Create model attribute to bind form data
        FlatShopRecord record = new FlatShopRecord();
        model.addAttribute("record",record);
        return "new_record";
    }

    @PostMapping("/saveRecord")
    public String saveRecord(@ModelAttribute("record") FlatShopRecord record)
    {
        FlatShopRecord form_data = service.saveRecord(record);
        System.out.println(form_data);
        return "redirect:/";
    }

    @GetMapping("/showUpdateForm/{id}")
    public String updateForm(@PathVariable(value = "id") long id, Model model)
    {
        FlatShopRecord record = service.getRecordById(id);
        model.addAttribute("record",record);
        return "update_record";
    }

    @GetMapping("/deleteRecord/{id}")
    public String deleteRecord(@PathVariable(value = "id") Long id)
    {
        service.deleteRecordById(id);
        return "redirect:/";
    }

}
