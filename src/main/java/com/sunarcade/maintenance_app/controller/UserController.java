package com.sunarcade.maintenance_app.controller;

import com.sunarcade.maintenance_app.services.UserService;
import com.sunarcade.maintenance_app.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/registration")
public class UserController
{
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("user")
    protected UserRegistrationDto userRegistrationDto()
    {
        return new UserRegistrationDto();
    }

    @GetMapping
    protected String showRegistrationForm()
    {
        return "registration";
    }

    @PostMapping
    protected String registerUserAccount(@ModelAttribute("user")UserRegistrationDto userRegistrationDto)
    {
        userService.save(userRegistrationDto);
        return "redirect:/registration?success";
    }
}
