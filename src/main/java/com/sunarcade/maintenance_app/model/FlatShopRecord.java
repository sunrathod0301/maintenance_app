package com.sunarcade.maintenance_app.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "flat_shop_record")
public class FlatShopRecord
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String flatShopNo;
    private Double sinkingFund;
    private Double repairingFund;
    private Double serviceCharges;
    private Double parkingCharge;
    private Double interest;
    private Double developmentFund;
    private Double nonOccupancy;
    private Double educationTrainingFund;
    private Double total;
    @Column(name = "interest_after_15_days")
    private Double interestAfter15Days;
    @Column(name = "total_after_15_days")
    private Double totalAfter15Days;
    private Double numberOfUnpaidMonths;

}
