package com.sunarcade.maintenance_app.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table
public class Role
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public Role(String role_user) {
        this.name = role_user;
    }
}
