package com.sunarcade.maintenance_app.web.dto;

import lombok.Data;

@Data
public class UserRegistrationDto
{
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
