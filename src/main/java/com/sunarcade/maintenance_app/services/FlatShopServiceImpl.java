package com.sunarcade.maintenance_app.services;

import com.sunarcade.maintenance_app.model.FlatShopRecord;
import com.sunarcade.maintenance_app.repository.FlatShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FlatShopServiceImpl implements FlatShopService
{
    @Autowired
    private FlatShopRepository repository;

//    @Override
//    public Page<FlatShopRecord> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection)
//    {
//        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name())?
//                Sort.by(sortField).ascending():
//                Sort.by(sortField).descending();
//
//        if (sortDirection.equalsIgnoreCase(Sort.Direction.DESC.name())) {
//            sort = sort.descending();
//        }
//
//        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
//        return repository.findAll(pageable);
//    }
@Override
public Page<FlatShopRecord> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
    Sort sort = createSort(sortField, sortDirection);
    Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
    return repository.findAll(pageable);
}

    private Sort createSort(String sortField, String sortDirection) {
        Sort.Direction direction = sortDirection.equalsIgnoreCase(Sort.Direction.DESC.name()) ? Sort.Direction.DESC : Sort.Direction.ASC;
        return Sort.by(direction, sortField);
    }

    @Override
    public List<FlatShopRecord> getAllFlatShops() {
        return repository.findAll();
    }

    @Override
    public FlatShopRecord saveRecord(FlatShopRecord record) {
        return repository.save(record);
    }

    @Override
    public FlatShopRecord getRecordById(Long id) {
        Optional<FlatShopRecord> optional = repository.findById(id);
        FlatShopRecord record =null;
        if(optional.isPresent())
            record = optional.get();
        else
            throw new RuntimeException("No record present with id :"+ id);

        return record;
    }

    @Override
    public void deleteRecordById(Long id) {
        repository.deleteById(id);
    }
}
