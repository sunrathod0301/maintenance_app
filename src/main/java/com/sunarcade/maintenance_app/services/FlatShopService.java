package com.sunarcade.maintenance_app.services;

import com.sunarcade.maintenance_app.model.FlatShopRecord;
import org.springframework.data.domain.Page;

import java.util.List;

public interface FlatShopService
{
    List<FlatShopRecord> getAllFlatShops();
    FlatShopRecord saveRecord(FlatShopRecord record);
    FlatShopRecord getRecordById(Long id);
    void deleteRecordById(Long id);
    Page<FlatShopRecord> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
