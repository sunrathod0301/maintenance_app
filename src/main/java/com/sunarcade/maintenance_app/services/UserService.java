package com.sunarcade.maintenance_app.services;

import com.sunarcade.maintenance_app.model.User;
import com.sunarcade.maintenance_app.web.dto.UserRegistrationDto;

public interface UserService
{
    User save(UserRegistrationDto userRegistrationDto);
}
